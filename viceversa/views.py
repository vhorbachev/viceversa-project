from django.shortcuts import render


def home(request):
    return render(request, 'home.html')


def len_(user_text):            #len(user_text.split())
    count_words =0
    flag = 0
    for i in range(len(user_text)):
        if user_text[i] != ' ' and flag == 0:
            count_words += 1
            flag = 1
        elif user_text[i] == ' ':
            flag = 0
    return str(count_words)

def reverse(request):
    user_text = request.GET['usertext']
    countwords = len_(user_text)
    reversed_text = user_text[::-1]
    return render(request, 'reverse.html', {'usertext':user_text, 'reversedtext':reversed_text, 'countwords':countwords})


